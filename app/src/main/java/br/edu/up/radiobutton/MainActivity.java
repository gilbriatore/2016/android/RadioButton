package br.edu.up.radiobutton;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

//    final RadioButton radioButton1sim = (RadioButton) findViewById(R.id.rbtRedes1sim);
//    final RadioButton radioButton1nao = (RadioButton) findViewById(R.id.rbtRedes1nao);
//    final RadioButton radioButton2dados = (RadioButton) findViewById(R.id.rbtRedes2dados);
//    final RadioButton radioButton2email = (RadioButton) findViewById(R.id.rbtRedes2email);
//    final RadioButton radioButton3sim = (RadioButton) findViewById(R.id.rbtRedes3sim);
//    final RadioButton radioButton3nao = (RadioButton) findViewById(R.id.rbtRedes3nao);
//    final RadioButton radioButton4cinco = (RadioButton) findViewById(R.id.rbtRedes4cinco);
//    final RadioButton radioButton4sete = (RadioButton) findViewById(R.id.rbtRedes4sete);
//    final RadioButton radioButton5tres = (RadioButton) findViewById(R.id.rbtRedes5tres);
//    final RadioButton radioButton5cinco = (RadioButton) findViewById(R.id.rbtRedes5cinco);
//
//    Button Resposta = (Button)findViewById(R.id.btnRespostaRedes);
//
//    EditText cxacertos = (EditText)findViewById(R.id.editText);
//    EditText cxerros = (EditText)findViewById(R.id.editText2);
//
//    Resposta.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClickView(View v)
//      {
//        int qnt_acertos=0, qnt_erros=0;
//
//        if (radioButton1sim.isChecked())
//        {
//          qnt_acertos=qnt_acertos+1;
//        }
//      }
//    });

  }


  String[] respostas = new String[5];

  public void mostrarRepostas(View v){

    StringBuilder sb = new StringBuilder();
    for (String str : respostas) {
      if (str != null) {
        sb.append(str + ", ");
      }
    }

    Toast.makeText(this, sb.toString(), Toast.LENGTH_LONG).show();
  }

  public void onRadioButtonClicked(View view) {

    RadioButton rb = (RadioButton) view;
    String str = rb.getText().toString();

    switch(view.getId()) {
      case R.id.rbtRedes1nao:
      case R.id.rbtRedes1sim:
          respostas[0] = str;
          break;
      case R.id.rbtRedes2dados:
      case R.id.rbtRedes2email:
          respostas[1] = str;
          break;
      case R.id.rbtRedes3sim:
      case R.id.rbtRedes3nao:
          respostas[2] = str;
          break;
      case R.id.rbtRedes4cinco:
      case R.id.rbtRedes4sete:
          respostas[3] = str;
          break;
      case R.id.rbtRedes5tres:
      case R.id.rbtRedes5cinco:
          respostas[4] = str;
          break;
    }
  }
}